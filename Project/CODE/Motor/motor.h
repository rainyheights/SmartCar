#ifndef __MOTOR_H
#define __MOTOR_H

#include "headfile.h"

#define LEFT_DIR P07
#define RIGHT_DIR P05

extern int16 Left_Speed;         //左边电机脉冲计数值
extern int16 Right_Speed;        //右边电机脉冲计数值

extern int16 last_Left_Speed;
extern int16 last_Right_Speed;

extern int16 Speed_fork;
extern int16 Ring_distance;
extern int16 home_distance;

extern float Right_Dif_Speed[80];
extern float Left_Dif_Speed[80];
void Motor_init(void);
void Encoder_init(void);
void motor(int pwm1,int pwm2);
void GetMotorPulse(void);
#endif
