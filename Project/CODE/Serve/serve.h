#ifndef __SERVE_H
#define __SERVE_H

#include "headfile.h"

#define serve_center 735
extern float peremeter_serve[90];

typedef struct Pid
{
   float offset;
   float old_offset;
	 float old_old_offset;
	 int speed;
   float Kp;
   float Ki;
   float Kd;
   int P_value;
   int I_value;
   int D_value;
   int ControlOutNew;
   int ControlOutOld;
   int ControlOut;
   int Rate;
	 int Pwm;
	 int integral;
}Pid;


extern int16 Chuku_Count; 
extern int8  Chuku_flag;

extern Pid serve;
extern int16 pwmx;
void serve_init(void);
//void serve_control(void);
void serve_pwm(int pwm3);
void inportpwm(void);
void Chuku();
	
#endif