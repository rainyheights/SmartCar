#include "menu.h"
#include "headfile.h"
//uint8 i=0;//2023.4.8初始化为0

//void Main_Interface(void)       //主菜单界面
//{

//  int8 line_cursor = 0;//光标指向行数
//  uint8 cancel_confirm_flag = 1;//确认标志
//  int line_num=8;//界面行数
//	uint8 i=0;
//	
//  
//  oled_fill(0x00);
//  oled_p6x8str(0,0,"->Run");     
//  oled_p6x8str(0,1,"  Serve");   
//  oled_p6x8str(0,2,"  Show");
//  oled_p6x8str(0,3,"  Balance");
//  oled_p6x8str(0,4,"  Speed");
//  oled_p6x8str(0,5,"  Direction");
//	oled_p6x8str(0,6,"  Coil");
//	oled_p6x8str(0,7,"  ramp"); 


//  while(cancel_confirm_flag !=0)  //直到按下确认键
//  {			
//    if(KEY_Up==0)  //向上键按下，箭头上移
//    {
//      delay_ms(40);
//      if(KEY_Up==0)
//      {
//         line_cursor--;
//         if(line_cursor<0)
//            line_cursor=line_num-1;      //行数
//         for (i=0; i<line_num; i++)//箭头光标的移动
//         {
//            if (i == line_cursor)
//            {
//                oled_p6x8str(i, 0, "->");
//            }
//            else
//            {
//                oled_p6x8str(i, 0, "  ");
//            }
//         }
//         //Beep();
//         while(KEY_Up==0);  //当按键松开    
//      }
//    }
//    
//    if(KEY_Down==0)  //向下键按下，光标下移
//    {
//      delay_ms(40);
//      if(KEY_Down==0)
//      {
//         line_cursor++;
//         if(line_cursor>line_num-1)
//            line_cursor=0;      //行数
//         for (i = 0; i<line_num; i++)//箭头光标的移动
//         {
//            if (i == line_cursor)
//            {
//                oled_p6x8str(i, 0, "->");
//            }
//            else
//            {
//                oled_p6x8str(i, 0, "  ");
//            }
//         }
//        // Beep();
//         while(KEY_Down==0);  //当按键松开
//      }
//    }
//    
//    if(KEY_Confirm==0)  //确认键按下
//    {
//      delay_ms(40);
//      if(KEY_Confirm==0)
//      {
//        cancel_confirm_flag = 0; 
//       // Beep();
//        Select(line_cursor);  
//        while(KEY_Confirm==0);  //当按键松开
//      }
//    }
//		
//  }
//	
//}

/*
 * 下列代码已经于2023.4.8日选择注释掉*/
//
//void Select(uint8 i)       //子菜单选择
//{
//    switch(i)
//    {
//      case (0):
//        oled_fill(0x00);
//        break;
//      case (1):
//       Mode_Select();  
//        break;
//      case (2):
//        Show_Select();
//        break;
//      case (3):
//       Blance_Select();  
//        break;
//      case (4):
//        Speed_Select();  
//        break;
//      case (5):
//        Direction_Select();  
//        break;
//			case (6):
//        Coil_Bias();
//        break;
//     case (7):
//        Ramp_Value();
//        break;
//    }

//添加一下代码可能可以 消除 报错，但不一定 变量未使用！！！
//UNREFERENCED_PARAMETER(i);
  
//}

void Speed_Select(void)
{
//    static float nuit=1;        //修改单位长度

//    int8 line_cursor = 0;//光标指向行数
//    uint8 cancel_confirm_flag = 1;//确认标志
//    int line_num=8;//界面行数
//    uint8 i=0;
//    oled_fill(0x00);
//    oled_p6x8str(0,0,"->Nuit :"); 
//    oled_p6x8str(0,1,"  Speed:");
// 
//    oled_printf_float(60,0,nuit,3,3);
//    oled_printf_float(60,1,Expect_Speed,3,3);

//    
//    while(cancel_confirm_flag !=0)  //直到按下确认键
//    {
//      if(KEY_Up==0)  //向上键按下，箭头上移
//      {
//        delay_ms(40);
//        if(KEY_Up==0)
//        {
//          line_cursor--;
//          if(line_cursor<0)
//            line_cursor=line_num-1;      //行数
//          for ( i = 0; i<line_num; i++)//箭头光标的移动
//          {
//              if (i == line_cursor)
//              {
//                  oled_p6x8str(i, 0, "->");
//              }
//              else
//              {
//                  oled_p6x8str(i, 0, "  ");
//              }
//          }
//         // Beep();
//          while(KEY_Up==0);  //当按键松开  
//        }
//      }
//    
//      if(KEY_Down==0)  //向下键按下，箭头下移
//      {
//        delay_ms(40);
//        if(KEY_Down==0)
//        {
//          line_cursor++;
//          if(line_cursor>line_num-1)
//              line_cursor=0;      //行数
//          for (i = 0; i<line_num; i++)//箭头光标的移动
//          {
//              if (i == line_cursor)
//              {
//                  oled_p6x8str(i, 0, "->");
//              }
//              else
//              {
//                  oled_p6x8str(i, 0, "  ");
//              }
//          }
//          //Beep();
//          while(KEY_Down==0);  //当按键松开  
//        }
//      }
//			
//      if(KEY_Decrease==0)  //自减键按下
//      {
//          delay_ms(40); 
//          if(KEY_Decrease==0)
//          {
//              switch (line_cursor)  //光标所在的行数减1
//              {
//                case 0:
//                  nuit=nuit/10;
//                  oled_printf_float(0,10,nuit,3,3);
//                  break;
//                }
//               // Beep();
//                while(KEY_Decrease==0);  //当按键松开   
//          }    
//      }
//    
//    
//      if(KEY_Add==0)  //自加键按下
//      {
//          delay_ms(40); 
//          if(KEY_Add==0)
//          {
//              switch (line_cursor)  //光标所在的行数减1
//              {
//                case 0:
//                  nuit=nuit*10;
//                  oled_printf_float(0,10,nuit,3,3);
//                  break;
//                }
//                //Beep();
//                while(KEY_Add==0);  //当按键松开   
//          }    
//      }    
//    
//      if(KEY_Confirm==0)  //确认键按下
//      {
//        delay_ms(40);
//        if(KEY_Confirm==0)
//        {
//          cancel_confirm_flag = 0; 
//         // Beep();              
//          while(KEY_Confirm==0);  //当按键松开
//        }
//      }
//  }
  
}  


void key_test(void)
{
	
	oled_fill(0x00);

    if(KEY_Up==0)  //
    {
      delay_ms(40);
      if(KEY_Up==0)
      {
         oled_uint16(0,0,1);
         while(KEY_Up==0);  //当按键松开        
      }
    }
		
	 if(KEY_Down==0)  //
    {
      delay_ms(40);
      if(KEY_Down==0)
      {
         oled_uint16(0,1,2);
         while(KEY_Down==0);  //当按键松开        
      }
    }
		
			 if(KEY_Left==0)  //
    {
      delay_ms(40);
      if(KEY_Left==0)
      {
         oled_uint16(0,2,3);
         while(KEY_Left==0);  //当按键松开        
      }
    }
		
	 if(KEY_Right==0)  //
    {
      delay_ms(40);
      if(KEY_Right==0)
      {
         oled_uint16(0,3,4);
         while(KEY_Right==0);  //当按键松开        
      }
    }
		
}
void key_init(void)
{
	gpio_mode(P0_0,GPIO);
	gpio_mode(P0_1,GPIO);
	gpio_mode(P0_2,GPIO);
	gpio_mode(P0_3,GPIO);
	
	gpio_pull_set(P0_0,PULLUP);
	gpio_pull_set(P0_1,PULLUP);
	gpio_pull_set(P0_2,PULLUP);
	gpio_pull_set(P0_3,PULLUP);
	
	KEY_Up=1;
	KEY_Down=1;
	KEY_Left=1;
	KEY_Right=1;
}
