#ifndef __INDUCTANCE_H
#define __INDUCTANCE_H
#include "headfile.h"

extern int16 ADCData[7];
extern int16 ADCData_last[7];

extern int16 ADC_left[10];
extern int16 ADC_right[10];
extern int16 ADC_right_middle[10];
extern int16 ADC_left_middle[10];
extern int16 ADC_middle[10];
extern int16 ADC_left_back[10];
extern int16 ADC_right_back[10];

extern int32 ADCsum_left;
extern int32 ADCsum_right;
extern int32 ADCsum_middle;
extern int32 ADCsum_left_middle;
extern int32 ADCsum_right_middle;
extern int32 ADCsum_left_back;
extern int32 ADCsum_right_back;

extern int16 ADC_average_left ;
extern int16 ADC_average_right;
extern int16 ADC_average_middle;
extern int16 ADC_average_right_middle;
extern int16 ADC_average_left_middle;
extern int16 ADC_average_left_back;
extern int16 ADC_average_right_back;

extern float ERROR_L;
extern float ERROR_R;
extern float ERROR_LV;
extern float ERROR_RV;
extern float ERROR_M;
extern float ERROR_LB;
extern float ERROR_RB;

extern float ERROR_Ringleft;
extern float ERROR_Ringright;
extern float ERROR_Fork;

extern float ERROR_ALL;
extern int16 ERROR_ALL_s;

extern int8 flag;

extern float point_center1;
extern float point_center1_old[10];

void Inductance_Init(void);
void Get_ADC(void);
void InductorNormal(void);
#endif

