#include "inductance.h"

/**********************ADC初始值***********************/
int16 ADCData[7]={0,0,0,0,0,0,0};
int16 ADCData_last[7]={0,0,0,0,0,0,0};

/******************电感采集值递推数组******************/ 
int16 ADC_left[10]  				=	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_right[10] 				=	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_left_middle[10]   =	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_right_middle[10]  =	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_middle[10]			  =	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_left_back[10]		  =	{0,0,0,0,0,0,0,0,0,0}	;
int16 ADC_right_back[10] 	 	=	{0,0,0,0,0,0,0,0,0,0}	;

/********************电感和值**************************/
int32 ADCsum_left =0;
int32 ADCsum_right=0;
int32 ADCsum_left_middle =0;
int32 ADCsum_right_middle=0;
int32 ADCsum_middle=0;
int32 ADCsum_left_back=0;
int32 ADCsum_right_back=0;

/**************电感递推后的加权平均值*****************/     
int16 ADC_average_left =0;
int16 ADC_average_right=0;
int16 ADC_average_left_middle =0;
int16 ADC_average_right_middle =0;
int16 ADC_average_middle=0;
int16 ADC_average_left_back=0;
int16 ADC_average_right_back=0;

/*****************加权递推有关******************/
int32 Weighted_Ration_T[10]={793,110,35,18,14,10,8,6,4,2};
int32 weight_sum=1000;

/*******************电感值差和比*******************/
uint16 left_right_inductor_sum =0;   //和
int16  left_right_inductor_sub =0;   //差

uint16 left_right_middle_inductor_sum =0;   //和
int16  left_right_middle_inductor_sub =0;   //差

int8 cfq_count=0;

float point_center1=0;
float point_center1_old[10]={0,0,0,0,0,0,0,0,0,0};

/******************电感归一化之后的值****************/
float ERROR_L=0.0;
float ERROR_R=0.0;
float ERROR_M=0.0;
float ERROR_LV=0.0;
float ERROR_RV=0.0;
float ERROR_LB=0.0;
float ERROR_RB=0.0;

/******************用于循迹的误差********************/
float ERROR_ALL=0.0;
float ERROR_ALL_Last=0.0;

/********************圆环循迹************************/
float ERROR_Ringleft=0.0;
float ERROR_Ringright=0.0;

/********************三岔循迹(暂时没用上)************************/
float ERROR_Fork=0.0;

/*****************循迹误差放大一百倍*****************/
int16 ERROR_ALL_s=0;

/****电感初始化****/
void Inductance_Init(void)
{
	adc_init(ADC_P10,ADC_SYSclk_DIV_2);
	adc_init(ADC_P11,ADC_SYSclk_DIV_2);
	adc_init(ADC_P13,ADC_SYSclk_DIV_2);
	adc_init(ADC_P14,ADC_SYSclk_DIV_2);
	adc_init(ADC_P15,ADC_SYSclk_DIV_2);
	adc_init(ADC_P16,ADC_SYSclk_DIV_2);
	adc_init(ADC_P17,ADC_SYSclk_DIV_2);
}
void Get_ADC(void)
{
	short k;
	//ADC采集
	ADCData[0] =  adc_once(ADC_P10, ADC_12BIT);
	ADCData[1] =  adc_once(ADC_P11, ADC_10BIT);
	ADCData[2] =  adc_once(ADC_P13, ADC_10BIT);
	ADCData[3] =  adc_once(ADC_P14, ADC_10BIT);
	ADCData[4] =  adc_once(ADC_P15, ADC_10BIT);
	ADCData[5] =  adc_once(ADC_P16, ADC_10BIT);
	ADCData[6] =  adc_once(ADC_P17, ADC_12BIT);

	//简单进行7路ADC一阶滤波0.7-0.3效果好一点，用几路就小于几
	for(k=0;k<7;k++)
	{
		ADCData[k]=0.7*ADCData[k]+0.3*ADCData_last[k];
		ADCData_last[k]=ADCData[k];
	}
}

/*************************************************************************
 *  函数名称：void InductorNormal(void)
 *  功能说明：采集电感电压并并归一化；
 *  函数返回：无
 *************************************************************************/
void InductorNormal(void)
{
	int i;
	Get_ADC();

	ADC_left[0]  = ADCData[0];																	//l1	
	ADC_left_middle[0] = ADCData[2];														//l2
	ADC_left_back[0]=ADCData[1];																//lb
	ADC_middle[0]=ADCData[3];																		
	ADC_right_back[0]=ADCData[4];																//rb
	ADC_right_middle[0]=ADCData[5];															//r2
	ADC_right[0]=ADCData[6];																		//r1
	
	
/********************************************加权平均滤波********************************************/
	
	/************************先进行历史数组递推************************/
		for (i=8;i>=0;i--)          
     {
         ADC_left[i+1]=ADC_left[i];
         ADC_right[i+1]=ADC_right[i];
         ADC_left_middle[i+1]=ADC_left_middle[i];
         ADC_right_middle[i+1]=ADC_right_middle[i];
			   ADC_middle[i+1]=ADC_middle[i];
				 ADC_left_back[i+1]=ADC_left_back[i];
			   ADC_right_back[i+1]=ADC_right_back[i];
     }

		/************************加权求和************************/
		 for (i=0;i<10;i++)
     {
         ADCsum_left  			 += ADC_left[i] 				* Weighted_Ration_T[i];
         ADCsum_right 			 += ADC_right[i] 				* Weighted_Ration_T[i];
			   ADCsum_left_middle  += ADC_left_middle[i]  * Weighted_Ration_T[i];
         ADCsum_right_middle += ADC_right_middle[i] * Weighted_Ration_T[i];
			   ADCsum_middle       += ADC_middle[i] 			* Weighted_Ration_T[i];
			   ADCsum_left_back    += ADC_left_back[i] 		* Weighted_Ration_T[i];
			   ADCsum_right_back   += ADC_right_back[i]	  * Weighted_Ration_T[i];
     }
		 
		 /************************除以权值算平均************************/
		 ADC_average_left  				= ( ADCsum_left         /  weight_sum);
		 ADC_average_right			  = ( ADCsum_right        /  weight_sum);
		 ADC_average_left_middle  = ( ADCsum_left_middle  /  weight_sum);
		 ADC_average_right_middle = ( ADCsum_right_middle /  weight_sum);
		 ADC_average_middle 			= ( ADCsum_middle       /  weight_sum);
		 ADC_average_left_back		= ( ADCsum_left_back    /  weight_sum);
		 ADC_average_right_back	  = ( ADCsum_right_back   /  weight_sum);
		 
/****************************************对处理之后的数据进行归一化（手动标定）****************************************/
//		ERROR_L		= 100	* ( ADC_average_left					- 5.0 )	/	( 2300.0	-	5.0 )	;
//		ERROR_LV	= 100	* ( ADC_average_left_middle		- 0.0 )	/	( 500.0	-	0.0 )	;
//		ERROR_LB	=	100	* ( ADC_average_left_back			- 0.0 ) / ( 470.0	-	0.0 )	;
//		ERROR_M		= 100	* ( ADC_average_middle				- 0.0 )	/	( 490.0	-	0.0 )	;		 
//		ERROR_RB	=	100	*	( ADC_average_right_back		- 0.0	) / ( 500.0	-	0.0 )	;
//		ERROR_RV	= 100	* ( ADC_average_right_middle 	- 0.0 )	/	( 520.0	-	0.0 )	;
//		ERROR_R		= 100	* ( ADC_average_right					- 86.0 )	/	( 2700.0	-	86.0 )	;		 
		 
		 
		ERROR_L		= 100	* ( ADC_average_left					- 5.0 )	/	( 2200.0	-	5.0 )	;
		ERROR_LV	= 100	* ( ADC_average_left_middle		- 0.0 )	/	( 510.0	-	0.0 )	;
		ERROR_LB	=	100	* ( ADC_average_left_back			- 0.0 ) / ( 470.0	-	0.0 )	;
		ERROR_M		= 100	* ( ADC_average_middle				- 0.0 )	/	( 520.0	-	0.0 )	;		 
		ERROR_RB	=	100	*	( ADC_average_right_back		- 0.0	) / ( 480.0	-	0.0 )	;
		ERROR_RV	= 100	* ( ADC_average_right_middle 	- 0.0 )	/	( 540.0	-	0.0 )	;
		ERROR_R		= 100	* ( ADC_average_right					- 86.0 )	/	( 2600.0	-	86.0 )	;		
		 
/****************************************归一化限幅****************************************/	
/***************大于100好像没有影响先不限幅***************/
	if(ERROR_L<=0)	ERROR_L=0;
	if(ERROR_R<=0)	ERROR_R=0;
	if(ERROR_LV<=0)	ERROR_LV=0;
	if(ERROR_RV<=0)	ERROR_RV=0;
	if(ERROR_M<=0)	ERROR_M=0;
	if(ERROR_LB<=0)	ERROR_LB=0;
	if(ERROR_RB<=0)	ERROR_RB=0;	

/********************************************************************************出道保护*******************************************************************************************/	 
	if(ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back<=7)
	{
		stop_flag=1;
//		cfq_count++;
//		if(cfq_count>=5)
//		{
//			if(ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back<=8)
//			{
//				stop_flag=1;
//				cfq_count=0;
//			}
//			else		
//				cfq_count=0;				
//		}	
	}

///********************************************************************************基础循迹*******************************************************************************************/	
//	if((ERROR_LB-ERROR_RB>=10)||(ERROR_RB-ERROR_LB>=10))										//简单的弯道判断   																																														 (本人太菜，直线调的很抖，才出此下策)
//	{
//		
//		if(ERROR_LB>ERROR_RB)
//		{
//			ERROR_ALL   = 100*((0.75*(ERROR_L-ERROR_R)+0.25*(ERROR_LB-ERROR_RB))/(0.75*(ERROR_L+ERROR_R)+0.25*(ERROR_LB-ERROR_RB)));
//			
//			/*******************************************左右转不一致，放大右转误差，减小左转误差*******************************************/          //参数有待调整
//			if(ERROR_ALL<0)
//			{
//				ERROR_ALL = 1.20*ERROR_ALL;
//			}
//			
//			else
//			{
//				ERROR_ALL = 0.98*ERROR_ALL;
//			}
//		}
//		
//		else if(ERROR_LB<=ERROR_RB)
//		{
//			ERROR_ALL   = 100*((0.75*(ERROR_L-ERROR_R)+0.25*(ERROR_LB-ERROR_RB))/(0.75*(ERROR_L+ERROR_R)-0.25*(ERROR_LB-ERROR_RB)));
//			
//			/*******************************************左右转不一致，放大右转误差，减小左转误差*******************************************/
//			if(ERROR_ALL<0)
//			{
//				ERROR_ALL = 1.20*ERROR_ALL;
//			}
//			
//			else
//			{
//				ERROR_ALL = 0.98*ERROR_ALL;
//			}
//		}
//	}
//	
//	
///**********************************************通过缩小直道的偏差值解决直道抖动的情况**********************************************/
//	
//	else if((ERROR_LB-ERROR_RB<10)&&(ERROR_RB-ERROR_LB<10))										//简单的直道判断  
//	{
//		if(ERROR_LB>ERROR_RB)
//		{
//			ERROR_ALL   = 100*((0.80*(ERROR_L-ERROR_R)+0.20*(ERROR_LB-ERROR_RB))/(0.80*(ERROR_L+ERROR_R)+0.20*(ERROR_LB-ERROR_RB)));
//			
//			ERROR_ALL		=	0.45*ERROR_ALL;
//		}
//		
//		else if(ERROR_LB<=ERROR_RB)
//		{
//			ERROR_ALL   = 100*((0.80*(ERROR_L-ERROR_R)+0.20*(ERROR_LB-ERROR_RB))/(0.80*(ERROR_L+ERROR_R)-0.20*(ERROR_LB-ERROR_RB)));
//			
//			ERROR_ALL		=	0.45*ERROR_ALL;
//		}
//		
//	}

/********************************************************************************基础循迹*******************************************************************************************/	

															/*************************************************出库到入环*************************************************/	
if((ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back>=100)&&(EX0_COUNT==0)&&ring_time==0)
{
		ERROR_ALL   = 128*((0.60*(1.00*ERROR_LV-ERROR_RV)+0.40*(1.00*ERROR_LB-ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
}

															/*************************************************出环到停车线*************************************************/
if((ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back>=100)&&(EX0_COUNT==0)&&ring_time==1)
{
		ERROR_ALL   = 128*((0.60*(0.90*ERROR_LV-1.10*ERROR_RV)+0.40*(0.90*ERROR_LB-1.10*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
}

															/*************************************************过停车线到出三岔*************************************************/
else if((ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back>=100)&&(EX0_COUNT==1)&&fork_open==1)
{
		ERROR_ALL   = 128*((0.60*(0.80*ERROR_LV-ERROR_RV)+0.40*(0.80*ERROR_LB-ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
}

															/*************************************************出三岔到停车*************************************************/
else if((ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back>=100)&&(EX0_COUNT==1)&&fork_open==0)
{
		ERROR_ALL   = 128*((0.60*(0.90*ERROR_LV-1.00*ERROR_RV)+0.40*(0.90*ERROR_LB-1.00*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
}

	
/****************************************数据累加和记得清零，不然出大问题****************************************/
		ADCsum_left 				 =  0;
		ADCsum_right				 =  0;
		ADCsum_left_middle	 =	0;
		ADCsum_right_middle  =	0;
		ADCsum_middle				 =	0;
		ADCsum_left_back		 =	0;
		ADCsum_right_back		 =	0;
}

