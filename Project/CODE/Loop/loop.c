#include "loop.h"

int8 ring_step=0;				//圆环步骤
int16 countr=0;					//入环打角时间
int8 ring_time=0;				//进环次数
int16 ring_count=0;			//第二次出环后计数，后方舵机打角
int8 ring_flag=0;				//圆环标志位，打角结束后陀螺仪开始积分
int8 ring_speed=0;			//环内差速标志位

void circle_check(void)
{
	if(ring_time==0)
	{
		switch(ring_step)
		{ 
			/******************************************************预入环检测******************************************************/
			
			case no_ring :
			
			if((ERROR_L>=34&&ERROR_R>=69
				&&ERROR_M>=77&&ERROR_LV>=78&&ERROR_RV>=75)
			||(ERROR_L<=13&&ERROR_R>=84&&ERROR_LV<=80&&ERROR_RV>=80&&ERROR_M>=71))			
			{
				ring_step=right_ring;
			}
     
			break;
    
			/******************************************************入环点检测******************************************************/			
			
			case right_ring:  
     
				Straight_speed=70;
				Expect_speed=70;
				ERROR_ALL   = 128*((0.70*(1.00*ERROR_LV-0.85*ERROR_RV)+0.30*(1.00*ERROR_LB-0.85*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
			
				/******************编码器积分******************/
				if(Ring_distance>=3200)//3000
				{
					ring_step=pre_right_ring;
					Ring_distance=0;
				}
				
			break;
    
			/******************************************************入环时舵机打角******************************************************/				
				
			case pre_right_ring:
				
				ring_speed=1;
				ring_flag=1;
				Straight_speed=75;
				Expect_speed=75;
				Servo_PWM=670;
				countr++;			
				if(countr>=90)
				{
					ring_step=in_right_ring;
					countr=0;
				}
  
			break;
    
			/******************************************************环内循迹******************************************************/	
				
			case in_right_ring:
				
					P36=0;
//					ERROR_ALL   = -0.95*ERROR_RB;
			
			ERROR_ALL   = -1.00*ERROR_RB;
					Straight_speed=90;
					Expect_speed=90; 
			
					/*************陀螺仪Z轴角度积分*************/
					if(mpu6050_gyro_z>=290)
					{
						ring_step=pre_out_right_ring;
					}
					
			break;    
    
			/******************************************************积分出环，纠正轨迹******************************************************/		
					
			case pre_out_right_ring:
				
					P36=1;	 
					Servo_PWM=710;
					Straight_speed=92;
					Expect_speed=92;
			
					if(mpu6050_gyro_z>=325)
					{
						ring_speed=0;
						ring_step=out_ring;					
					}
    
			break;
    
			/******************************************************已出环******************************************************/	
					
			case out_ring:
				
				ERROR_ALL   = 128*((0.70*(1.00*ERROR_LV-ERROR_RV)+0.30*(1.00*ERROR_LB-ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
				P36=1;

				if(mpu6050_gyro_z>=340)
				{
					Straight_speed=85;
				  Expect_speed=82;
					ring_step=no_ring;	
					ring_flag=0;
					ring_time=1;
					loop_open=0;
					EX0=1;
					mpu6050_gyro_z=0;
				}
				
			break;
		}
	}
	
	
	
	else if(ring_time==1)
	{
		switch(ring_step)
		{ 
			/******************************************************预入环检测******************************************************/
			
			case no_ring :
			
//					if((ERROR_L>=34&&ERROR_R>=69
//						&&ERROR_M>=77&&ERROR_LV>=78&&ERROR_RV>=75)
//						||(ERROR_L<=13&&ERROR_R>=84&&ERROR_LV<=80&&ERROR_RV>=80&&ERROR_M>=71))
			
								if((ERROR_L>=20&&ERROR_R>=69
						&&ERROR_M>=77&&ERROR_LV>=72&&ERROR_RV>=75)
						||(ERROR_L<=13&&ERROR_R>=84&&ERROR_LV<=80&&ERROR_RV>=80&&ERROR_M>=71))
						{			
							ring_step=right_ring;
						}
     
			break;
    
			/******************************************************入环点检测******************************************************/				
						
			case right_ring:  
     
				Straight_speed=70;
				Expect_speed=70;
				ERROR_ALL   = 128*((0.70*(1.00*ERROR_LV-0.95*ERROR_RV)+0.30*(1.00*ERROR_LB-0.95*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
			
				if(Ring_distance>=2850)//2450
				{
					ring_step=pre_right_ring;
					Ring_distance=0;
				}
    
			break;
    
			/******************************************************入环时舵机打角******************************************************/	
				
			case pre_right_ring:
				
				ring_speed=1;
				Servo_PWM=660;
				ring_flag=1;
				Straight_speed=80;
				Expect_speed=80;			
				countr++;			
				if(countr>=90)
				{
					ring_step=in_right_ring;
					countr=0;
				}
  
			break;
    
			/******************************************************环内循迹******************************************************/	
				
			case in_right_ring:

				P36=0;	
//				ERROR_ALL   = -0.95*ERROR_RB;
			ERROR_ALL   = -1.00*ERROR_RB;
				Straight_speed=92;
				Expect_speed=92;  
			
				if(mpu6050_gyro_z>=285)
				{
					ring_step=pre_out_right_ring;
				}
				
			break;    
    
			/******************************************************积分出环，纠正轨迹******************************************************/
				
			case pre_out_right_ring:
				
				P36=1;
				Straight_speed=92;
				Expect_speed=92;
				Servo_PWM=710;
				
				if(mpu6050_gyro_z>=325)
				{
					ring_speed=0;
					ring_step=out_ring;					
				}
    
			break;
				
			/******************************************************已出环******************************************************/	
    
			case out_ring:
				
				ERROR_ALL   = 128*((0.70*(1.00*ERROR_LV-ERROR_RV)+0.30*(1.00*ERROR_LB-ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
						
				if(mpu6050_gyro_z>=335)
				{
					Straight_speed=85;
				  Expect_speed=82;
					ring_flag=0;
					ring_step=no_ring;
					EX0=1;				
					loop_open=0;
					ring_time=3;
					mpu6050_gyro_z=0;
					loop_car=1;
				}
				
			break;
		}
	}	
	
}

















