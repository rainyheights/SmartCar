#ifndef _LOOP_H
#define _LOOP_H
#include "headfile.h"


#define      no_ring                 0
#define      right_ring              1
#define      pre_right_ring          2
#define      in_right_ring           3
#define      pre_out_right_ring      4
#define      out_ring                5

extern int8 ring_flag;
extern int8 ring_speed;
extern int8 ring_step;
extern int16 ring_count;
extern int8 ring_time;

void circle_check(void);

	
#endif

