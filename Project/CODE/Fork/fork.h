#ifndef _FORK_H
#define _FORK_H
#include "headfile.h"

extern int8 Fork;       			//三岔判断标志，为1时不判断
extern int8 Fork_left;				//左三岔标志，0表示没走过左三岔
extern int8 Fork_left_out;   //左三岔出

extern int8 Fork_right;			//右三岔
extern int8 Fork_right_out;

extern int8 fork_to_ring;

extern int8 Fork_finish_left;

extern int8 Fork_angle;
extern int8 ramp_flag;

void fork_f(void);
//void ramp(void);

#endif