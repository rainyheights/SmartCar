#include "fork.h"
#include "headfile.h"
//自定义了变量，不一定可行！！！

int16 EX0_COUNT;
int16 mpu_gyro_x,mpu_gyro_y,mpu_gyro_z;
float angle_up;


//自定义了变量，不一定可行！！！

int8 Fork=0;       					//三岔判断标志，为1时不判断

int8 Fork_left=0;						//左三岔标志，0表示没走过左三岔
int8 Fork_left_out=0;  			//左三岔出，该标志用于进入右三岔时的判断       其实更像一个出三岔路的预备条件

int8 Fork_right=0;					//右三岔
int8 Fork_right_out=0;			//右三岔出，用于开启右三岔的出三岔检测    		 其实更像一个出三岔路的预备条件

/***********************防止在三岔中继续识别到三岔***********************/
int8 Fork_finish_left=0;		//左三岔出，检测到三岔出口发生变化
int8 Fork_finish_right=0;		//右三岔出，检测到三岔出口发生变化

int8 Fork_angle=0;				//出三岔打角标志位

int8 ramp_flag=0;					//坡道检测标志位

int16 ramp_count=1;
int16 ramp_count_r=0;

void ramp(void)
{
	if(ADC_average_left+ADC_average_left_middle+ADC_average_right_middle+ADC_average_middle+ADC_average_right_back+ADC_average_left_back<=140)
	{
		stop_flag=1;	
	}
}

void fork_f(void)
{	
/*******************************************************************************左三岔*******************************************************************************/
/*************************************************************************三岔判断条件*************************************************************************/
//	if(ERROR_L<=22&&ERROR_R<=28
//		&&ERROR_LV<=60&&ERROR_RV<=48
//		&&ERROR_LV>=20&&ERROR_RV>=9
//		&&((ERROR_LB+ERROR_RB)>=5)&&((ERROR_LB+ERROR_RB)<=52)
//		&&ERROR_M<=61&&ERROR_M>=30
//		&&Fork==0&&Fork_left==0&&Fork_finish_left==0&&ring_step==0)
	
		if(ERROR_L<=15&&ERROR_R<=28
		&&ERROR_LV<=40&&ERROR_RV<=40
		&&ERROR_LV>=5&&ERROR_RV>=2
		&&((ERROR_LB+ERROR_RB)>=5)&&((ERROR_LB+ERROR_RB)<=40)
		&&ERROR_M<=45&&ERROR_M>=10
		&&Fork==0&&Fork_left==0&&Fork_finish_left==0&&ring_step==0)	
	
	{
		P36=0;
		Fork=1;
		Fork_left=1;
	}

/*****************检测到三岔之后打角*****************/
	if(Fork==1&&Fork_left==1&&Fork_left_out==0)
	{
		Straight_speed=30;
		Expect_speed=30;
		Servo_PWM=800;
		/********打角结束开始正常循迹********/
		if(mpu6050_gyro_z<=-30)
		{
			ERROR_ALL   = 128*((0.60*(0.75*ERROR_LV-1.00*ERROR_RV)+0.40*(0.75*ERROR_LB-1.00*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
			P36=1;
			Straight_speed=75;
			Expect_speed=75;
			Fork=0;		
			mpu6050_gyro_z=0;
			Fork_left_out=1;
//			ramp_flag=1;
		}
	}
	
/*********开始检测出三岔的条件*********/		
	if(Fork_left_out==1)
	{		
		/******************************************************纠正轨迹******************************************************/	
		ERROR_ALL   = 128*((0.60*(0.75*ERROR_LV-1.00*ERROR_RV)+0.40*(0.75*ERROR_LB-1.00*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
		if(angle_up>=6&&ramp_count==1)
		{
			ramp_flag=1;
			ramp_count=0;
		}
		if(ramp_flag==1)
		{
			P36=0;
			Straight_speed=30;
			Expect_speed=30;
			ERROR_ALL   = 128*((0.60*(0.75*ERROR_LV-1.00*ERROR_RV)+0.40*(0.75*ERROR_LB-1.00*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
			if(angle_up<=2)
			{		P36=1;
				ramp_flag=0;
				Straight_speed=75;
				Expect_speed=75;
			}
		}
//		ramp_count++;
//		/******************************************************坡道减速******************************************************/
//	  if(ramp_count>=30&&ramp_flag==1)
//		{
//			P36=0;
//			Straight_speed=55;
//			Expect_speed=55;				
//			ramp_count_r++;
//			if(ramp_count_r>=200)
//			{
//				ramp_count=0;
//				P36=1;	
//				ERROR_ALL   = 128*((0.60*(0.60*ERROR_LV-1.00*ERROR_RV)+0.40*(0.60*ERROR_LB-1.00*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
//				Straight_speed=80;
//				Expect_speed=75;
//				ramp_flag=0;	
//				ramp_count_r=0;				
//			}
//		}
		/******************************************************纠正轨迹******************************************************/
		if(ramp_flag==0&&ramp_count==0)
		{
			P36=0;	
				Straight_speed=75;
				Expect_speed=75;
				ERROR_ALL   = 128*((0.60*(0.90*ERROR_LV-1.10*ERROR_RV)+0.40*(0.90*ERROR_LB-1.10*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
		}	
		/******************************************************判断三岔出口******************************************************/		
//		if((ERROR_LB+ERROR_RB)>=150&&ramp_flag==0)
		if((ERROR_LB+ERROR_RB)>=85&&ramp_flag==0)
		 {	
		 
				Fork_angle=1;			 
		 }
		 if(Fork_angle==1)
		 {
			 Servo_PWM=800;
			 /**********************陀螺仪积分摆正车身**********************/
//				if(gyroz<=-32)


//雨山修改：原先变量：gyro_z
			 if(mpu6050_gyro_z<=-1)
				{
					ramp_flag=1;
					P36=1;
					Fork_finish_left=1;
					Fork_left_out=0;
					Fork_left=0;
					fork_open=0;
					loop_open=1;
					Straight_speed=75;
					Expect_speed=75;	
					Fork_angle=0;				
					mpu6050_gyro_z=0;
			}
		 }
	}
	
/*******************************************************************************右三岔*******************************************************************************/
/*************************************************************************三岔判断条件*************************************************************************/
//		if(ERROR_L<=22&&ERROR_R<=28
//		&&ERROR_LV<=60&&ERROR_RV<=48
//		&&ERROR_LV>=20&&ERROR_RV>=9
//		&&((ERROR_LB+ERROR_RB)>=5)&&((ERROR_LB+ERROR_RB)<=52)
//		&&ERROR_M<=61&&ERROR_M>=30
//		&&Fork==0&&Fork_finish_left==1&&Fork_right==0&&ring_step==0&&EX0_COUNT==1)		

		if(ERROR_L<=5&&ERROR_R<=28
		&&ERROR_LV<=40&&ERROR_RV<=40
		&&ERROR_LV>=5&&ERROR_RV>=2
		&&((ERROR_LB+ERROR_RB)>=5)&&((ERROR_LB+ERROR_RB)<=40)
		&&ERROR_M<=45&&ERROR_M>=10
		&&Fork==0&&Fork_finish_left==1&&Fork_right==0&&ring_step==0&&EX0_COUNT==1)
	{
		P36=0;
		Fork=1;
		Fork_right=1;
	}
	
/*****************检测到三岔之后打角*****************/
	if(Fork==1&&Fork_right==1&&Fork_right_out==0)
	{
		
		Straight_speed=40;
		Expect_speed=40;
		Servo_PWM=640;
		/********打角结束开始正常循迹********/
		if(mpu6050_gyro_z>=30)  //快速时80
		{			
			Straight_speed=90;
			Expect_speed=85;
			Fork=0;		
			Fork_right_out=1;

		}
	}
	
/*********开始检测出三岔的条件*********/		
	if(Fork_right_out==1)
	{		
		Straight_speed=90;
		Expect_speed=85;		
		ERROR_ALL   = 100*((0.60*(1.00*ERROR_LV-1.10*ERROR_RV)+0.40*(1.00*ERROR_LB-1.10*ERROR_RB))/(1.00*(ERROR_LV+ERROR_RV)));
		if((ERROR_LB+ERROR_RB)>=120)
		{
			P36=1;
			Fork_finish_right=1;
			Fork_right_out=0;							
		}		
	}	
	
/*********两次三岔完成，所有三岔有关标志清零*********/                      //主要是太懒了，不想找哪些在前面清过了
	if(Fork_finish_right==1)
	{  
		Fork_angle=1;
		Servo_PWM=640;
		if(mpu6050_gyro_z>=26)
			if(mpu6050_gyro_z>=2)
		{
			Fork_finish_left=0; 
			Fork_right=0;		
			ramp_flag=0;			
			loop_open=1;
			fork_open=0;
			Fork_finish_right=0;	
			Straight_speed=75;
			Expect_speed=75;
			Fork_angle=0;
			mpu6050_gyro_z=0;
		}
	}	

}