#ifndef _CONTROL_H_
#define _CONTROL_H_

#include "headfile.h"

typedef struct MOTOR_PID
{
    float Ki;
    float Kp;
    float Kd;
    float Speed_Error;//速度偏差
    float Speed_Error1;//速度上一次偏差
    float Speed_Error2;//速度上上次偏差    
    float Speed_Error3;//速度变化率    Speed_Error-Speed_Error1
    float SetSpeed;//期望速度
       
    float pwm_p;        //比例项
    float pwm_i;        //积分项
    float pwm_d;        //微分项
    float ErrorIntegral;
    
    float pwmOutNew;
    float pwmOutOld;
    float wheelSpeed;
    float lastWheelSpeed;

}Motor_pid;

typedef struct SERVO_PID     //舵机PID参数   位置式PD
{
   float Kp;        //比例常数
   float Kp_a;
   float Kd;        //微分常数
   float NowError;  //当前偏差  Error(n)-Error(n-1)
   float LastError; //偏差前项  Error(n-1)-Error(n-2)
   float Error_deviation;//本次偏差减上次偏差的结果   微分项
   float output;
   float output_old[5];
}Servo_pid;

extern Motor_pid pid_R;
extern Motor_pid pid_L;
extern int servo_pwm_old[10];
extern int16 Servo_PWM;

extern int16 stop_flag;
extern int16 ex0_stop_flag;
extern int Expect_speed;
extern int Straight_speed;
extern float Car_Speed;

extern int16 PWM_Left;
extern int16 PWM_Right;

extern int8  home_flag;
extern int8  loop_car;
extern int16 fork_open_count;
extern int16 fork_open;

extern int8  loop_open;
void datainit();
void Car_Control();
void Inprot(void);
void Outprot(void);
int MotorPI(short i,int16 Encoder,int16 Target);
float PID_Servo(Servo_pid *pd);
void Sevor_Control();
void motor_control();
void motor_speed_set();
void speed_set();
void BEEP(void);

#endif

