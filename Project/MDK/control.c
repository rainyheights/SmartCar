#include "control.h"

Motor_pid pid_R,pid_L;//定义左右轮PID
Servo_pid Servo;//定义舵机PID
Servo_pid Ring;
Servo_pid	Turn;

Pid M_left; 
Pid M_Right;

int Expect_speed=75;//转弯减速速度  75
float Curvature_number=0;    //转弯陀螺仪曲率系数
float Curvature=0;
int Straight_speed=75;//直道速度

int8 stop_inport=0;

float Curve_count=0.6;//差速系数      暂时没用上

int16 Servo_PWM=0;
int16 PWM_Servo=0;
int16 PWM_Servo_Last=0;
int16 PWM_Left=0;
int16 PWM_Right=0;

int8  loop_car=0;
int8  home_flag=0;

int16 stop_count=0;

int16 fork_open_count=0;
int16 fork_open=0;

int8 	home_return=0;

int8  loop_open=0;

int16 stop_flag=0;  //出界停车标志位

int16 ex0_stop_flag=0;  //霍尔元件停车标志位
int16 ex0_Stop_count=0;   
int16 ex0_Stop_delay=0;  //停车标志置1后，用该计数延长库角距离
int8  ex0_Inport_Flag=0;
int16 ex0_Inport_count=0;

int8  home_turn=0;				//入库打角标志

float pid_Kp,pid_Ki,pid_Kd;
int   servo_pwm_old[10]={0,0,0,0,0,0,0,0,0,0};            //保存舵机PD输出值  用于差速
int   servo_pwm_last[10]={0,0,0,0,0,0,0,0,0,0};            //保存舵机PD输出值  用于差速


void datainit()
{
	pid_L.SetSpeed= 0;
  pid_R.SetSpeed= 0;
	
	M_left.Kp=250;
	M_left.Ki=1.6;
	
	M_left.Pwm=0;
	M_left.offset=0;
	M_left.old_offset=0;
	M_left.integral =0;
	
	M_Right.Kp=250;
	M_Right.Ki=1.7;
	
	M_Right.Pwm=0;
	M_Right.offset=0;
	M_Right.old_offset=0;
	
/*******正常循迹舵机PID*******/
	Servo.Kp=0.60;  //0.68
	Servo.Kd=0.25;

/*******圆环循迹舵机PID*******/
//	Ring.Kp=0.66;
	Ring.Kp=0.71;
	Ring.Kd=0.00;	
//  60  0.55
/*******三岔循迹舵机PID*******/     //暂时没用上
	Turn.Kp=1.10;
	Turn.Kd=0.20;
}
void Car_Control()
{
		InductorNormal();     //传感器采集
		GetMotorPulse();
    mpu6050_angle_updata();
		
	/********************陀螺仪积分********************/
	if(ring_flag==1||loop_car==1||Fork==1||Fork_angle==1)
	{
		get_gyro_z();
	}
	
	/********************锁死三岔********************/
	if(fork_open==1)
	{
	  fork_f();		
	}
	
	/********************锁死圆环********************/
	if(loop_open==1)
	{
		circle_check();	
	}
	
	/********************第二次出圆环，陀螺仪记录角度********************/
	if(loop_car==1&&mpu6050_gyro_z>=590)
	{
		home_flag=1;
		mpu6050_gyro_z=0;
	}
	
	/********************第二次出圆环，编码器记路程********************/
	if(home_distance>=8400)//7650
	{
		P36=1;
		home_turn=1;
	}
		
	  Sevor_Control();
	  motor_control();
}

void Sevor_Control()
{
	int8 i;
	
/**********************************正常循迹**********************************/
	if(Fork==0&&(ring_step==0||ring_step==1||ring_step==5)&&Fork_right_out==0&&Fork_left_out==0&&Fork_angle==0)
	{		
		Servo.NowError=ERROR_ALL;
		Servo_PWM=PID_Servo(&Servo);
	
		for(i=9;i>0;i--)    //保存舵机值用于打角
		{
			servo_pwm_last[i]=servo_pwm_last[i-1];
		}
			servo_pwm_last[0]=(int)Servo_PWM;
		
			Servo_PWM=serve_center+PWM_Servo;
		
			if(Servo_PWM < serve_center-75)     //舵机限幅
				Servo_PWM = serve_center-75;
			if(Servo_PWM > serve_center+75)
				Servo_PWM = serve_center+75; 
	}
	
	if(ring_step==3)
	{
		Ring.NowError=ERROR_ALL;
		Servo_PWM=PID_Servo(&Ring);
	
		for(i=9;i>0;i--)    //保存舵机值用于打角
		{
			servo_pwm_last[i]=servo_pwm_last[i-1];
		}
			servo_pwm_last[0]=(int)Servo_PWM;
		
			Servo_PWM=serve_center+PWM_Servo;
		
			if(Servo_PWM < serve_center-75)     //舵机限幅
				Servo_PWM = serve_center-75;
			if(Servo_PWM > serve_center+75)
				Servo_PWM = serve_center+75; 		
	}
	
	if((Fork_right_out==1||Fork_left_out==1)&&Fork_angle==0)
	{
		Turn.NowError=ERROR_ALL;
		Servo_PWM=PID_Servo(&Turn);
	
		for(i=9;i>0;i--)    //保存舵机值用于打角
		{
			servo_pwm_last[i]=servo_pwm_last[i-1];
		}
			servo_pwm_last[0]=(int)Servo_PWM;
		
			Servo_PWM=serve_center+PWM_Servo;
		
			if(Servo_PWM < serve_center-75)     //舵机限幅
				Servo_PWM = serve_center-75;
			if(Servo_PWM > serve_center+75)
				Servo_PWM = serve_center+75; 		
	}	
	
	/*****************入库打角*****************/
	if(home_turn==1)
	{
		Servo_PWM=810;
	}
	
	if(home_return==1)
	{
		Servo_PWM=790;
	}
	
	
	if(ex0_Inport_Flag==1)  //前舵机向左打死
	{
			Servo_PWM=810;
	}
	
	/*****************出库打角*****************/
	if(Chuku_flag==1) 
	{
		Servo_PWM=810;
		Chuku();
	}
	
	/*****************第二次出环后计数打角*****************/
	if(ring_time==3)  //前瞻干簧管预放下标志位
	{
		ring_count++;
		if(ring_count>=500)
		{
			stop_inport=1;  //前瞻干簧管放下
			ring_count=0;
			ring_time=0;
		}
	}
	
	/*****************后方舵机打角*****************/	
	if(stop_inport==1)	
	{
		pwmx=1150;   //后侧小舵机打角放下干簧管
		inportpwm();
	}
			
	serve_pwm(Servo_PWM);
}


void motor_control()
{
	
	pid_L.wheelSpeed=Left_Speed;         //数值更新
	pid_R.wheelSpeed=Right_Speed;

	motor_speed_set();									//差速计算    和下面判断语句必须开启一个

/************不加差速就开启这个************/
//	if(stop_flag==0)
//	{
//			pid_L.SetSpeed=40;
//			pid_R.SetSpeed=40;
//	}
//	else
//	{
//			pid_L.SetSpeed=0;
//			pid_R.SetSpeed=0;
//	}
	
	/******************************倒车入库*****************************/
//		if(ex0_stop_flag==1)
//			{	
//					ex0_Inport_Flag=1;  //干簧管检测到停车标志，与ex0_stop_flag同
//					P36=1;
//				ex0_Stop_delay++;
//				if(ex0_Stop_delay>2)
//				{
//							P01=1;
//							P02=1;
//							P03=1;
//						M_left.integral=0;
//						M_Right.integral=0;
//						pid_L.SetSpeed=0;
//						pid_R.SetSpeed=0;
//					  ex0_Stop_count++;
//					  if(ex0_Stop_count>65)
//							{
//								ex0_Stop_delay=0;
//								ex0_Stop_count=0;
//								ex0_Inport_Flag=1;
//								ex0_stop_flag=0;
//							}
//				}
//			}
//			if(ex0_Inport_Flag==1)  //直接向左
//			{
//				stop_count++;
//				pid_R.SetSpeed=0;
//				pid_L.SetSpeed=0;
//				if(stop_count>=0)
//				{					
//					ex0_Inport_count++;
//					pid_R.SetSpeed=120;
//					pid_L.SetSpeed=0;				
//					P36=0;
//					if(ex0_Inport_count>38)
//					{
//						stop_count=0;
//						pid_L.SetSpeed=0;
//						pid_R.SetSpeed=0;
//					}	
//				}					
//											
//			}
			
			
			/*****************霍尔直接入库*****************/
			if(stop_inport==1)
			{
					P36=0;
					Straight_speed=80;
					Expect_speed=80;
					stop_inport=0;		
			}
			
			/*****************编码器计数入库*****************/
			if(home_turn==1)
			{
					pid_R.SetSpeed=90;
					pid_L.SetSpeed=5;	
					ex0_Inport_count++;
				if(ex0_Inport_count>=28)
				{	
					home_return=1;					
					pid_R.SetSpeed=0;
					pid_L.SetSpeed=0;	
				}
				
			}
				
//		speed_set();
//			pid_L.SetSpeed=50;
//			pid_R.SetSpeed=50;
		  PWM_Left  = MotorPI(2,Left_Speed, (int16)pid_L.SetSpeed);
	    PWM_Right = MotorPI(1,Right_Speed,(int16)pid_R.SetSpeed);
	
			motor(PWM_Left,PWM_Right);           //电机闭环
	
}
void motor_speed_set()
{
		  int speed;
			
			speed = (int)Expect_speed;     //太懒了，不想改成int型

/********************************************差速计算********************************************/	

			if(stop_flag==0)
			{
				/**********************环外差速**********************/	
				if(ring_speed==0)
				{
									/**********************差速与舵机打角联系在一起，打角越大差速越大**********************/	
					if(PWM_Servo>=0)         //舵机打角  分两个方向判断
					{
							if( PWM_Servo>18)   //左转
							{																				
//								pid_L.SetSpeed = speed-0.45*(int)(speed*PWM_Servo/75.0);//0.22 内轮减  75
//								pid_R.SetSpeed = speed+0.35*(int)(speed*PWM_Servo/75.0);	 //	 外轮加			

//									pid_L.SetSpeed = speed-0.45*(int)(speed*PWM_Servo/75.0);//0.22 内轮减
//									pid_R.SetSpeed = speed+0.15*(int)(speed*PWM_Servo/75.0);	 //	 外轮加					
									pid_L.SetSpeed = speed-0.40*(int)(speed*PWM_Servo/75.0);//0.22 内轮减
									pid_R.SetSpeed = speed+0.15*(int)(speed*PWM_Servo/75.0);	 //	 外轮加										
								
							}
					
							else if(PWM_Servo>=0 && PWM_Servo<=18)
							{

								pid_R.SetSpeed = Straight_speed;
								pid_L.SetSpeed = Straight_speed;
								
								if(pid_R.SetSpeed <=20)		pid_R.SetSpeed =20;
								if(pid_L.SetSpeed <=20)		pid_R.SetSpeed =20;
							}
					}
					
					else if(PWM_Servo<0)
					{

							 if(PWM_Servo<-18)   //右转
							{
								
//								pid_L.SetSpeed = speed-0.35*(int)(speed*PWM_Servo/75.0); //外轮加   75
//								pid_R.SetSpeed = speed+0.45*(int)(speed*PWM_Servo/75.0); //内轮减
								
//									pid_L.SetSpeed = speed-0.15*(int)(speed*PWM_Servo/75.0); //外轮加
//									pid_R.SetSpeed = speed+0.45*(int)(speed*PWM_Servo/75.0); //内轮减
								
																	pid_L.SetSpeed = speed-0.15*(int)(speed*PWM_Servo/75.0); //外轮加
									pid_R.SetSpeed = speed+0.40*(int)(speed*PWM_Servo/75.0); //内轮减

							}

							
							else if(PWM_Servo<0 && PWM_Servo>=-18)
							{

								pid_R.SetSpeed = Straight_speed;
								pid_L.SetSpeed = Straight_speed;
								
								if(pid_R.SetSpeed <=10)		pid_R.SetSpeed =10;
								if(pid_L.SetSpeed <=10)		pid_R.SetSpeed =10;
							}
					}
				}			

				
				/**********************环内差速**********************/	
				if(ring_speed==1)
				{
				/**********************差速与舵机打角联系在一起，打角越大差速越大**********************/	
					if(PWM_Servo>=0)         //舵机打角  分两个方向判断
					{
							if( PWM_Servo>18)   //左转
							{																				
									pid_L.SetSpeed = speed-0.45*(int)(speed*PWM_Servo/75.0);//0.22 内轮减
									pid_R.SetSpeed = speed+0.25*(int)(speed*PWM_Servo/75.0);	 //	 外轮加																		
							}
					
							else if(PWM_Servo>=0 && PWM_Servo<=18)
							{
								pid_R.SetSpeed = Straight_speed;
								pid_L.SetSpeed = Straight_speed;
								
								if(pid_R.SetSpeed <=20)		pid_R.SetSpeed =20;
								if(pid_L.SetSpeed <=20)		pid_R.SetSpeed =20;
							}
					}
					
					else if(PWM_Servo<0)
					{

							 if(PWM_Servo<-18)   //右转
							{
								pid_L.SetSpeed = speed-0.25*(int)(speed*PWM_Servo/75.0); //外轮加
								pid_R.SetSpeed = speed+0.45*(int)(speed*PWM_Servo/75.0); //内轮减

							}
							
							else if(PWM_Servo<0 && PWM_Servo>=-18)
							{
								pid_R.SetSpeed = Straight_speed;
								pid_L.SetSpeed = Straight_speed;
								
								if(pid_R.SetSpeed <=10)		pid_R.SetSpeed =10;
								if(pid_L.SetSpeed <=10)		pid_R.SetSpeed =10;
							}
					}
				}	
				
		}
			
/**********************触发停车**********************/	
		else if(stop_flag==1)
		{
				pid_L.SetSpeed=0;
				pid_R.SetSpeed=0;
		}
}
int MotorPI(short i,int16 Encoder,int16 Target)   //增量式
{
	int pwm;
	switch (i)
	{
		case 1:
			{
				M_Right.offset=Target-Encoder;
				M_Right.integral += M_Right.offset;
				if(M_Right.integral >2500) M_Right.integral=2500;
				if(M_Right.integral <-2500) M_Right.integral=-2500;
				M_Right.Pwm = M_Right.Kp*M_Right.offset + M_Right.Ki * M_Right.integral+M_Right.Kd * (M_Right.offset-M_Right.old_offset);
//								
				if(M_Right.Pwm > 8000) M_Right.Pwm = 8000;         //输出限幅
				else if(M_Right.Pwm < -8000)M_Right.Pwm = -8000;
				 
				M_Right.old_old_offset=M_Right.old_offset;   
				M_Right.old_offset=M_Right.offset;                     //偏差更新 
				pwm=M_Right.Pwm;
			}break;
		case 2:
			{
				M_left.offset=Target-Encoder;
//				M_left.Pwm=M_left.Kp * (M_left.offset-M_left.old_offset) + M_left.Kd * (M_left.offset-2*M_left.old_offset+M_left.old_old_offset) + M_left.Ki * M_left.offset;
				M_left.integral += M_left.offset;
				if(M_left.integral >2500) M_left.integral=2500;
				if(M_left.integral <-2500) M_left.integral=-2500;
				M_left.Pwm = M_left.Kp*M_left.offset + M_left.Ki * M_left.integral+M_left.Kd * (M_left.offset-M_left.old_offset);
				
				if(M_left.Pwm > 8000) M_left.Pwm = 8000;         //输出限幅
				else if(M_left.Pwm < -8000)M_left.Pwm = -8000;
				
				M_left.old_old_offset=M_left.old_offset;   
				M_left.old_offset=M_left.offset;                     //偏差更新 
				pwm=M_left.Pwm;
			}break;
	}
	return pwm;
}

float PID_Servo(Servo_pid *pd)
{
	 int8 i;
	
   pd->Error_deviation =  pd->NowError - pd->LastError; //微分
	 pd->output          = (pd->Kp*pd->NowError) + (pd->Kd*pd->Error_deviation);
	
	 pd->LastError       =  pd->NowError;  	//更新
	
	 for(i=0;i<4;i++)
	 {
	 		pd->output_old[i] = pd->output_old[i+1];
	 }
	 
	 pd->output_old[4] = pd->output;
	 
	 PWM_Servo=pd->output;
  /*-------------保存舵机打角值 用于差速-------------*/
	 for(i=9;i>0;i--)
	 {
	 		servo_pwm_old[i]=servo_pwm_old[i-1];
	 }
	 servo_pwm_old[0]=(int)pd->output;

	return PWM_Servo;
}
void speed_set()
{
	if(KEY_Up==0)
		{
			pid_L.SetSpeed=0;
			pid_R.SetSpeed=0;	
		}
		if(KEY_Down==0)
		{
			pid_L.SetSpeed=120;
			pid_R.SetSpeed=120;	
		}
		if(KEY_Left==0)
		{
			pid_L.SetSpeed=-80;
			pid_R.SetSpeed=-80;	
		}
		if(KEY_Right==0)
		{
			pid_L.SetSpeed=-60;
			pid_R.SetSpeed=-60;	
		}
}
void Inprot(void)
{
	pid_R.SetSpeed=100;
	pid_L.SetSpeed=25;
	P36=0;
	if(ex0_Inport_count>25)
	{
		pid_L.SetSpeed=0;
		pid_R.SetSpeed=0;
	}
}



void Outprot(void)
{
}

void BEEP(void)
{
//	P36=0;
//	delay_ms(10);
//	P36=1;
	
}