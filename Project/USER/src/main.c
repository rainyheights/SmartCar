/*********************************************************************************************************************
 * COPYRIGHT NOTICE
 * Copyright (c) 2020,逐飞科技
 * All rights reserved.
 * 技术讨论QQ群：一群：179029047(已满)  二群：244861897(已满)  三群：824575535
 *
 * 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
 * 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
 *
 * @file       		main
 * @company	   		成都逐飞科技有限公司
 * @author     		逐飞科技(QQ790875685)
 * @version    		查看doc内version文件 版本说明
 * @Software 		MDK FOR C251 V5.60
 * @Target core		STC32G12K128
 * @Taobao   		https://seekfree.taobao.com/
 * @date       		2020-12-18
 ********************************************************************************************************************/

#include "headfile.h"
#define LED P52
//编码器数据采集
#define DIR_LEFT P50
#define DIR_RIGHT P51
#define PLUSE_LEFT CTM0_P34
#define PLUSE_RIGHT CTM3_P04
#define L1   ADC_P10  //电磁信号采集的宏定义引脚
int16 encode_left=0;
int16 encode_right=0;

//2023.4.8修改定义
int8 tim_count=0;
uint8 data_adc=0;
int count_flag=1;
/*
 * 系统频率，可查看board.h中的 FOSC 宏定义修改。
 * board.h文件中FOSC的值设置为0,则程序自动设置系统频率为33.1776MHZ
 * 在board_init中,已经将P54引脚设置为复位
 * 如果需要使用P54引脚,可以在board.c文件中的board_init()函数中删除SET_P54_RESRT即可
 */


void main()
{
		DisableGlobalIRQ();		//关闭总中断
		board_init();			// 初始化寄存器,勿删除此句代码。

	  P03=!P03;
	  P52=0;
     //	P25=!P25;
    //硬件方式初始化oled屏幕，显示相关加速度数据
    oled_init();
    oled_init_spi();

    //初始化陀螺仪数据采集
    mpu6050_init();
    ctimer_count_init(encode_left);
    ctimer_count_init(encode_right);

    //电磁信号采集
    adc_init(L1,ADC_SYSclk_DIV_2);//这样就采集到了电磁信号数据了

	  EnableGlobalIRQ();		//开启总中断

    while (1){

        //编写主循环
        //采集数据
        mpu6050_get_gyro();
        mpu6050_get_accdata();

        oled_fill_spi(1);
        //显示数据
        oled_putpixel_spi(8*13,0,mpu6050_gyro_x);
        oled_putpixel_spi(8*13,1,mpu6050_gyro_y);
        oled_putpixel_spi(8*13,2,mpu6050_gyro_z);
        oled_putpixel_spi(8*13,3,mpu6050_acc_x);
        oled_putpixel_spi(8*13,4,mpu6050_acc_y);
        oled_putpixel_spi(8*13,5,mpu6050_acc_z);

        oled_putpixel_spi(8*13,8,1);
        //高低电平来判断编码器采集数据的正负
        if(DIR_LEFT==1)encode_left=-encode_left; else encode_left=encode_left;
        ctimer_count_clean(encode_left);
        if(DIR_RIGHT==1)encode_right=-encode_right; else encode_right=-encode_right;
        ctimer_count_clean(encode_right);
        //清空数据
        //之后显示数据
        oled_putpixel_spi(8*13,6,encode_left);
        oled_putpixel_spi(8*13,7,encode_right);

        data_adc= adc_once(L1,ADC_8BIT);//数模转换一次，赋值数据给data_adc
        delay_ms(200);//延时刷新数据
    }

}


//void TM0_Isr() interrupt 1
//{
//	fork_open_count++;
//	if(fork_open_count>=200&&count_flag==1)
//	{
//		fork_open=1;
//		fork_open_count=0;
//		count_flag=0;

//	}

//		Car_Control();

//		if(Chuku_flag==1)
//		{
//			Chuku();
//		}
//}

